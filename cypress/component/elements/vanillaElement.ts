
/**
 * @emits wrapper-type
 */
export class VanillaCustomElement extends HTMLElement {
  private _template: HTMLTemplateElement
  private _onclick: EventListener | null = null

  get onclick () {
    return this._onclick
  }

  set onclick (value: EventListener | null) {
    if (this._onclick) {
      this.removeEventListener('click', this._onclick)
    }

    if (value) {
      this.addEventListener('click', value)
    }

    this._onclick = value
  }

  constructor () {
    super()

    this.onWrapperType = this.onWrapperType.bind(this)

    this._template = document.createElement('template')
    this._template.innerHTML = `
      <div id='wrapper' tabindex="0">
        <slot>I am some content!</slot>
      </div>
    `

    this.attachShadow({ mode: 'open' })
    this.shadowRoot?.appendChild(this._template.content.cloneNode(true))

    this.shadowRoot?.querySelector('#wrapper')?.addEventListener('keypress', this.onWrapperType)
  }

  connectedCallback () {
    if (this._onclick) {
      this.addEventListener('click', this._onclick)
    }
  }

  disconnectedCallback () {
    if (this._onclick) {
      this.removeEventListener('click', this._onclick)
    }
  }

  private onWrapperType () {
    const evt = new CustomEvent('wrapper-type')
    this.dispatchEvent(evt)
  }
}

customElements.define('vanilla-element', VanillaCustomElement)

declare global {
  interface HTMLElementTagNameMap {
    'vanilla-element': VanillaCustomElement;
  }
}
