import { html, LitElement, PropertyValueMap } from 'lit'
import { customElement, property, state } from 'lit/decorators.js'

/**
 * @emits wrapper-type
 */
@customElement('lit-element')
export class LitCustomElement extends LitElement {
  @state() private _clickCount = 0

  @property() onclick: EventListener | null = null

  constructor () {
    super()

    this.onWrapperType = this.onWrapperType.bind(this)
  }

  render () {
    return html`
      <div id='wrapper' @keypress=${this.onWrapperType} tabindex="0">
        <slot>I am some content!</slot>

        <span>Button has been clicked ${this._clickCount} times</span>
        <button @click=${() => this._clickCount++}>Click Me!</button>
      </div>
    `
  }

  protected update (changedProperties: PropertyValueMap<this>): void {
    super.update(changedProperties)

    if (changedProperties.has('onclick')) {
      const oldListener = changedProperties.get('onclick')
      if (oldListener) {
        this.removeEventListener('click', oldListener)
      }
      if (this.onclick) {
        this.addEventListener('click', this.onclick)
      }
    }
  }

  connectedCallback () {
    super.connectedCallback()

    if (this.onclick) {
      this.addEventListener('click', this.onclick)
    }
  }

  disconnectedCallback () {
    super.disconnectedCallback()

    if (this.onclick) {
      this.removeEventListener('click', this.onclick)
    }
  }

  private onWrapperType () {
    const evt = new CustomEvent('wrapper-type')
    this.dispatchEvent(evt)
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'lit-element': LitCustomElement;
  }
}
