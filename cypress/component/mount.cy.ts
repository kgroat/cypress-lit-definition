import { html } from 'lit'

import './elements/litElement'
import './elements/vanillaElement'

describe('mount', () => {
  it('should render default elements onto the page', () => {
    cy.mount(html`<div id='myElement'>foo</div>`)

    cy.get('#myElement').should('exist')
  })

  it('should render inside of [data-lit-root]', () => {
    cy.mount(html`<div>bar</div>`)
    cy.get('[data-lit-root]').should('exist')
  })

  it('should clean up after itself', () => {
    cy.get('[data-lit-root]').should('not.exist')
  })

  describe('lit element', () => {
    it('should render its children', () => {
      cy.mount(html`<lit-element></lit-element>`)

      cy.get('lit-element').shadow().find('#wrapper').should('exist')
    })

    it('should render with slot content', () => {
      const content = 'Look at me, I am in a slot!'
      cy.mount(html`<lit-element>${content}</lit-element>`)

      cy.get('lit-element').should('contain.text', content)
    })

    it('should respond to state updates', () => {
      cy.mount(html`<lit-element></lit-element>`)

      cy.get('lit-element').shadow().should('contain.text', 'Button has been clicked 0 times')
      cy.get('lit-element').shadow().find('button').click().click().click()
      cy.get('lit-element').shadow().should('contain.text', 'Button has been clicked 3 times')
    })

    it('should register properties', () => {
      cy.mount(html`<lit-element .onclick=${cy.spy().as('clickListener') as () => void}></lit-element>`)

      cy.get('lit-element').shadow().find('#wrapper').click()

      cy.get('@clickListener').should('have.been.called')
    })

    it('should listen to custom events', () => {
      cy.mount(html`<lit-element @wrapper-type=${cy.spy().as('typeListener') as () => void}></lit-element>`)

      cy.get('lit-element').shadow().find('#wrapper').type('{enter}')

      cy.get('@typeListener').should('have.been.called')
    })
  })

  describe('vanilla custom element', () => {
    it('should render its children', () => {
      cy.mount(html`<vanilla-element></vanilla-element>`)

      cy.get('vanilla-element').shadow().find('#wrapper').should('exist')
    })

    it('should render with slot content', () => {
      const content = 'Look at me, I am in a slot!'
      cy.mount(html`<vanilla-element>${content}</vanilla-element>`)

      cy.get('vanilla-element').should('contain.text', content)
    })

    it('should register properties', () => {
      cy.mount(html`<vanilla-element .onclick=${cy.spy().as('clickListener')}></vanilla-element>`)

      cy.get('vanilla-element').shadow().find('#wrapper').click()

      cy.get('@clickListener').should('have.been.called')
    })

    it('should listen to custom events', () => {
      cy.mount(html`<vanilla-element @wrapper-type=${cy.spy().as('typeListener') as () => void}></vanilla-element>`)

      cy.get('vanilla-element').shadow().find('#wrapper').type('{enter}')

      cy.get('@typeListener').should('have.been.called')
    })
  })
})
