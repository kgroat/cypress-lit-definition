FROM playwright/webkit:playwright-1.32.1

USER root

RUN apt update && apt install wget && apt clean && rm -rf /var/lib/apt/lists/*

ENTRYPOINT [""]
