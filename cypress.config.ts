import { defineConfig } from 'cypress'

export default defineConfig({
  component: {
    reporter: 'cypress-mochawesome-reporter',
    reporterOptions: {
      charts: true,
      embeddedScreenshots: true,
      inlineAssets: true
    },
    devServer: {
      framework: 'cypress-ct-lit-element' as string as 'react',
      bundler: 'vite',
      viteConfig: {}
    },
    experimentalWebKitSupport: true,
    setupNodeEvents (on) {
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      require('cypress-mochawesome-reporter/plugin')(on)
    }
  }
})
