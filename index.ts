/// <reference types="cypress" />

import { getContainerEl, setupHooks } from '@cypress/mount-utils'
import { render, TemplateResult, LitElement } from 'lit'

let wrapper: HTMLDivElement | null = null

function cleanup () {
  wrapper?.remove()
  wrapper = null
}

interface MountingOptions {
  log?: boolean
}

/**
 * Mounts a lit `html` template result into a component test.
 *
 * @param component The lit template
 * @param options An options hash, allowing you to disable logging
 * @returns The childrean mounted into the component test root.
 *
 * @example cy.mount(html`<div>content</div>`)
 */
export function mount (
  component: TemplateResult,
  { log = true }: MountingOptions = {}
) {
  cleanup()

  const container = getContainerEl()
  wrapper = document.createElement('div')
  wrapper.dataset.litRoot = ''
  container.appendChild(wrapper)

  render(component, wrapper)

  return cy
    .wrap(wrapper, { log: false })
    .children({ log: false })
    .then(async ($children) => {
      let done = false
      while (!done) {
        done = (await Promise.all([...$children].map(c => {
          if ('updateComplete' in c) {
            return (c as LitElement).updateComplete
          } else {
            return true
          }
        }))).reduce((a, b) => a && b)
      }

      log && Cypress.log({
        name: 'mount',
        message: 'Mounted component',
        $el: $children
      })

      return $children
    })
}

setupHooks(cleanup)
