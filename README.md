# Cypress Lit Component Test Definition
This is a custom [Lit](https://lit.dev/) component test definition for cypress.

[![npm](https://img.shields.io/npm/v/cypress-ct-lit-element?logo=npm)](https://badge.fury.io/js/cypress-ct-lit-element)
[![Pipeline status](https://gitlab.com/kgroat/cypress-ct-lit-element/badges/main/pipeline.svg)](https://gitlab.com/kgroat/cypress-ct-lit-element/pipelines/main/latest)
[![Electron status](https://gitlab.com/api/v4/projects/44599793/jobs/artifacts/main/raw/badge.svg?job=test%20electron)](https://gitlab.com/kgroat/cypress-ct-lit-element/-/jobs/artifacts/main/browse/cypress/reports/html?job=test%20electron)
[![Chrome status](https://gitlab.com/api/v4/projects/44599793/jobs/artifacts/main/raw/badge.svg?job=test%20chrome)](https://gitlab.com/kgroat/cypress-ct-lit-element/-/jobs/artifacts/main/browse/cypress/reports/html?job=test%20chrome)
[![Edge status](https://gitlab.com/api/v4/projects/44599793/jobs/artifacts/main/raw/badge.svg?job=test%20edge)](https://gitlab.com/kgroat/cypress-ct-lit-element/-/jobs/artifacts/main/browse/cypress/reports/html?job=test%20edge)
[![Firefox status](https://gitlab.com/api/v4/projects/44599793/jobs/artifacts/main/raw/badge.svg?job=test%20firefox)](https://gitlab.com/kgroat/cypress-ct-lit-element/-/jobs/artifacts/main/browse/cypress/reports/html?job=test%20firefox)
[![Webkit status](https://gitlab.com/api/v4/projects/44599793/jobs/artifacts/main/raw/badge.svg?job=test%20webkit)](https://gitlab.com/kgroat/cypress-ct-lit-element/-/jobs/artifacts/main/browse/cypress/reports/html?job=test%20electron)

![demo](https://gitlab.com/kgroat/cypress-ct-lit-element/-/raw/main/.gitlab/demo.gif?inline=true)

* [Getting started]
* [Manual setup]
  * [Update your Cypress config]
  * [Update your component config]
* [Usage]

[Getting started]: #getting-started
## Getting started
To install, run:
```bash
npm install -D cypress-ct-lit-element
```

Once you have the package installed alongside Cypress, you can run `npx cypress open`, choose "Component Testing", and Lit should appear in the list of frameworks available.

Learn more about [third-party definitions](https://docs.cypress.io/guides/component-testing/third-party-definitions)

[Manual setup]: #manual-setup
## Manual setup
If you already have an existing configuration and can't go through the setup process again, you can update your configuration to work with `cypress-ct-lit-element`.

Before you can get started, you need to also install `lit` and `vite`.  Once you have, you need to take two updates:

[Update your Cypress config]: #update-your-cypress-config
### Update your Cypress config
First, update your `cypress.config.{ts,js}` to have `'cypress-ct-lit-element'` as your framework and `'vite'` or `'webpack'` as your bundler:
```ts
export default defineConfig({
  component: {
    devServer: {
      framework: 'cypress-ct-lit-element',
      bundler: 'vite',
      // more config here
    }
  }
})
```

If you're using TypeScript, you may get a type error when setting the `framework` property.  If so, you'll need to typecast it as `any`:
```ts
framework: 'cypress-ct-lit-element' as any,
```

[Update your component config]: #update-your-component-config
### Update your component config
Next, add the following to your `cypress/component.{ts,js}` file:
```ts
import { mount } from 'cypress-ct-lit-element'

Cypress.Commands.add('mount', mount)
```

If you're using TypeScript, you may also need to add the following block to get the types to work:
```ts
declare global {
  namespace Cypress {
    interface Chainable {
      mount: typeof mount
    }
  }
}
```

Once you've followed these steps, you should be ready to write some tests!

[Usage]: #usage
## Usage
You can now mount any HTML in a component test, for example:
```ts
import { html } from 'lit'

it('should display content', () => {
  const text = 'I will show up in the test'
  cy.mount(html`<div id='content'>${text}</div>`)

  cy.get('#content').should('contain.text', text)
})
```

This also works with custom elements.  You can either reach into the shadow root manually:
```ts
import { html } from 'lit'

it('should render its children', () => {
  cy.mount(html`<my-element></my-element>`)

  cy.get('my-element')
    .shadow().find('.my-part') // you have to go through the shadow root to access elements inside <my-element>
    .should('exist')
})
```

Or, you can turn on `includeShadowDom` ([see the docs on configuring Cypress](https://docs.cypress.io/guides/references/configuration#Global))
```ts
export default defineConfig({
  component: {
    includeShadowDom: true
    // more config here
  }
})
```

With this option set, you can simply access elements in your custom elements directly:
```ts
import { html } from 'lit'

it('should render its children', () => {
  cy.mount(html`<my-element></my-element>`)

  cy.get('.my-part') // reaches through the shadow root to find element(s) inside <my-element>
    .should('exist')
})
```
